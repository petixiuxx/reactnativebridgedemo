//
//  Bulb.swift
//  Demo
//
//  Created by Harry Nguyen on 6/26/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

@objc(Bulb)
class Bulb: RCTViewManager {
  @objc
  override static func requiresMainQueueSetup() -> Bool {
    return true
  }
  override func view() -> UIView! {
    return BulbView()
  }
}
