import React, { useState } from 'react';
import {Platform, StyleSheet, requireNativeComponent, Text, View} from 'react-native';
const Bulb = requireNativeComponent("Bulb")

export default function App() {
  const [isOn, setIsOn] = useState(false)  
  const onStatusChange = (e) => {
    setIsOn(e.nativeEvent.isOn)
  }

  return (
      <View style={styles.container}>
        <View style={styles.top} >
          <Text>This state of Bulb come from Native Code to JavaScript</Text>
          <Text>{ isOn ? "Bulb is On" : "Bulb is Off"}</Text>
        </View>

          <Bulb 
            style={ styles.bottom } 
            isOn={isOn} 
            onStatusChange={onStatusChange}
          />
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  top: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  bottom: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});